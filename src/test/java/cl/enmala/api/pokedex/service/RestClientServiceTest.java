package cl.enmala.api.pokedex.service;

import cl.enmala.api.pokedex.dto.Species;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("dev")
class RestClientServiceTest {
    @Autowired
    private RestClientService restClientService;

    @Test
    void getEvolutionChain() {
        var chain = new HashMap<String,String>();
        chain.put("url","https://pokeapi.co/api/v2/evolution-chain/10/");
        var species = new Species(26, "raichu", chain);
        var evolution = restClientService.getEvolutionChain(species);
        assertTrue(evolution!=null);
        assertTrue(evolution.getChain().getSpeciesLink().getName().equalsIgnoreCase("pichu"));
    }
}