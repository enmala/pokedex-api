package cl.enmala.api.pokedex.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("dev")
class PokemonServiceTest {

    @Autowired
    private PokemonService pokemonService;

    @Test
    void getPokemonPage() {
    }

    @Test
    void getPokemon() {
        var p = pokemonService.getPokemon(25);
        assertTrue(p.isPresent());
        assertTrue(p.get().getName().equalsIgnoreCase("pikachu"));
        assertTrue(p.get().getTypes().size()==1);
        p = pokemonService.getPokemon(2500);
        assertTrue(p.isEmpty());
    }

    @Test
    void getPokemonDetail() {
        var p = pokemonService.getPokemonDetail(25);
        assertTrue(p.isPresent());

        assertTrue(p.get().getName().equalsIgnoreCase("pikachu"));
        assertTrue(p.get().getTypes().size()==1);
        assertTrue(!p.get().getEvolutions().isEmpty());
        p = pokemonService.getPokemonDetail(2500);
        assertTrue(p.isEmpty());
    }

    @Test
    void getPokemonPicture() {
        var i = pokemonService.getPokemonPicture(25);
        assertTrue(i.getStatusCode().is2xxSuccessful());
        assertTrue(i.hasBody());
        i = pokemonService.getPokemonPicture(2500);
        assertTrue(i.getStatusCode() == HttpStatus.NOT_FOUND);
    }

}