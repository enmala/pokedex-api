package cl.enmala.api.pokedex;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("dev")
class DemoAPIApplicationTests {

    @Test
    void contextLoads() {
    }

}
