package cl.enmala.api.pokedex.service;

import cl.enmala.api.pokedex.dto.PokemonDetail;
import cl.enmala.api.pokedex.dto.PokemonList;
import cl.enmala.api.pokedex.dto.PokemonResume;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface PokemonService {
    PokemonList getPokemonPage(int start, int count);

    Optional<PokemonResume> getPokemon(int id);

    @Cacheable(value = "pokemon",key = "{#id}", unless = "#result == null")
    Optional<PokemonDetail> getPokemonDetail(int id);

    ResponseEntity<byte[]> getPokemonPicture(int id);
}
