package cl.enmala.api.pokedex.service;

import cl.enmala.api.pokedex.dto.EvolutionChain;
import cl.enmala.api.pokedex.dto.PokePage;
import cl.enmala.api.pokedex.dto.PokemonResume;
import cl.enmala.api.pokedex.dto.Species;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
@Slf4j
public class RestClientService {

    private final static String PAGE_URL = "https://pokeapi.co/api/v2/pokemon/?offset=%d&limit=%d";
    private final static String POKEMON_URL = "https://pokeapi.co/api/v2/pokemon/%d/";

    private RestTemplate rest;
    private HttpHeaders headers;

    public RestClientService() {
        this.rest = new RestTemplate();
        this.headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
    }

    public PokePage getPage(int offset, int limit) {
        HttpEntity<String> requestEntity = new HttpEntity<String>("", headers);
        ResponseEntity<PokePage> responseEntity
                = rest.exchange(PAGE_URL.formatted(offset,limit), HttpMethod.GET, requestEntity, PokePage.class);
        return responseEntity.getBody();
    }

    public PokemonResume getResume(int id){
        HttpEntity<String> requestEntity = new HttpEntity<String>("", headers);
        PokemonResume pokemon;
        try {
            ResponseEntity<PokemonResume> responseEntity
                    = rest.exchange(POKEMON_URL.formatted(id), HttpMethod.GET, requestEntity, PokemonResume.class);
            pokemon = responseEntity.getBody();
        } catch (Exception e) {
           pokemon = null;
        }
        return pokemon;
    }

    public ResponseEntity<byte[]> getImage(String url){
        ResponseEntity<byte[]> response;
        var headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));
        HttpEntity<String> requestEntity = new HttpEntity<String>("", headers);
        try {
            response = rest.exchange(url, HttpMethod.GET, requestEntity, byte[].class);
        } catch (HttpClientErrorException e) {
            response = ResponseEntity.status(e.getStatusCode()).build();
        }
        return response;
    }

    public EvolutionChain getEvolutions(String url) {
        log.debug("url evoluciones: {}", url);
        EvolutionChain response= null;
        HttpEntity<String> requestEntity = new HttpEntity<String>("", headers);
        if(url!=null) {
            try {
                ResponseEntity<Species> speciesResponseEntity
                        = rest.exchange(url, HttpMethod.GET, requestEntity, Species.class);
                var species = speciesResponseEntity.getBody();
                if(species!=null && species.getEvolutionChainURL()!=null) {
                    response = getEvolutionChain(species);
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        return response;
    }

    public EvolutionChain getEvolutionChain(Species species) {
        EvolutionChain response;
        HttpEntity<String> requestEntity = new HttpEntity<String>("", headers);
        log.debug("Especie: {}", species.getId());
        ResponseEntity<EvolutionChain> evolutionChainResponseEntity
                = rest.exchange(species.getEvolutionChainURL(), HttpMethod.GET, requestEntity, EvolutionChain.class);
        response = evolutionChainResponseEntity.getBody();
        return response;
    }
}
