package cl.enmala.api.pokedex.service;

import cl.enmala.api.pokedex.dto.PokemonDetail;
import cl.enmala.api.pokedex.dto.PokemonList;
import cl.enmala.api.pokedex.dto.PokemonResume;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class PokemonServiceImpl implements PokemonService {

    private final RestClientService restClientService;

    @Override
    @Cacheable(value = "pokePage",key = "{#start,#count}")
    public PokemonList getPokemonPage(int start, int count) {
        log.debug("Lee página desde {}", start);
        PokemonList pokemonList = null;
        if(start>0 && count >0) {
            var page = restClientService.getPage(start-1,count);
            pokemonList = new PokemonList(page.getCount(),page.getPokemonList());
        }
        return pokemonList;
    }

    @Override
    @Cacheable(value = "pokemon",key = "{#id}", unless = "#result == null")
    public Optional<PokemonResume> getPokemon(int id) {
        log.debug("Obtiene resumen de pokemon {}", id);
        Optional<PokemonResume> response = Optional.empty();
        var pokemon = restClientService.getResume(id);
        if(pokemon!=null)
            response = Optional.of(pokemon);
        return response;
    }

    @Override
    @Cacheable(value = "pokemonDetail",key = "{#id}", unless = "#result == null")
    public Optional<PokemonDetail> getPokemonDetail(int id) {
        log.debug("Obtiene detalle de pokemon {}", id);
        Optional<PokemonDetail> response = Optional.empty();
        //PokemonDetail pokemon = PokemonDetail(restClientService.getResume(id));
        PokemonResume resume = restClientService.getResume(id);
        if(resume!=null) {
            PokemonDetail pokemon = new PokemonDetail(resume);
            if (pokemon != null) {
                pokemon.setEvolutionChain(restClientService.getEvolutions(pokemon.getSpeciesURL()));
                response = Optional.of(pokemon);
            }
        }
        return response;
    }

    @Override
    @Cacheable(value = "pokeImage",key = "{#id}",unless = "#result.statusCodeValue != 200")
    public ResponseEntity<byte[]> getPokemonPicture(int id){
        ResponseEntity<byte[]> response;
        log.debug("Get image of {}", id);
        var pokemon = getPokemon(id);
        if(pokemon.isPresent() && pokemon.get().getSprites()!=null) {
            response = restClientService.getImage(pokemon.get().getSprites().getFrontURL());
        } else
            response = ResponseEntity.notFound().build();
        return response;
    }

}
