package cl.enmala.api.pokedex.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PokemonDetail extends PokemonResume{
    private String description;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Schema(hidden = true)
    private EvolutionChain evolutionChain;

    public PokemonDetail(PokemonResume other){
        super(other);
    }

    public List<SpeciesLink> getEvolutions(){
        if(evolutionChain!=null) {
            var list = new ArrayList<SpeciesLink>();
            list.add(evolutionChain.getChain().getSpeciesLink());
            addLink(list,evolutionChain.getChain().getEvolution());
            return list;
        } else return null;
    }

    private void addLink(List<SpeciesLink> list, List<ChainLink> link ){
        link.forEach(evolution->{
            list.add(evolution.getSpeciesLink());
            addLink(list,evolution.getEvolution());
        });
    }

}
