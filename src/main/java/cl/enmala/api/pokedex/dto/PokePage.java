package cl.enmala.api.pokedex.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PokePage {
    private int count;
    private String next;
    private String previus;
    @JsonProperty("results")
    private List<Pokemon> pokemonList;
}
