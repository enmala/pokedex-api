package cl.enmala.api.pokedex.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Species {
    private int id;
    private String name;
    @JsonProperty(value = "evolution_chain",access = JsonProperty.Access.WRITE_ONLY)
    private Map<String,String> evolutionChain;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public String getEvolutionChainURL(){
        return evolutionChain.get("url");
    }
}