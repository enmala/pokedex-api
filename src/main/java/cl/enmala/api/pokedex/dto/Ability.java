package cl.enmala.api.pokedex.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ability {
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Schema(hidden = true)
    private int slot;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Map<String, String> ability;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    String getAbility(){
        return ability.get("name");
    }
}
