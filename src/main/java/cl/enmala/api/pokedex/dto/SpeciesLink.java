package cl.enmala.api.pokedex.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class SpeciesLink {
    private String name;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Schema(hidden = true)
    private String url;

    public int getId(){
        if(url!=null && url.split("/").length==7)
            return Integer.parseInt(url.split("/")[6]);
        else
            return 0;
    }
}
