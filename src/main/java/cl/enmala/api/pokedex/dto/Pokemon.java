package cl.enmala.api.pokedex.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pokemon {
    private String name;
    @Schema(hidden = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String url;
    private Integer id;

    public void setUrl(String url) {
        this.url = url;
        if(url!=null && url.split("/").length==7)
            this.id = Integer.parseInt(url.split("/")[6]);
    }

    protected Pokemon(Pokemon other){
        this.id = other.getId();
        this.url = other.getUrl();
        this.name = other.getName();
    }
}
