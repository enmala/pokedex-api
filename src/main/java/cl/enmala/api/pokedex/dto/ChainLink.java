package cl.enmala.api.pokedex.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChainLink {
    @JsonProperty("evolves_to")
    private List<ChainLink> evolution;
    @JsonProperty("species")
    private SpeciesLink speciesLink;
}
