package cl.enmala.api.pokedex.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PokemonResume extends Pokemon {

    private List<Type> types;
    private List<Ability> abilities;
    private int weight;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Schema(hidden = true)
    private Sprites sprites;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Schema(hidden = true)
    private Map<String,String> species;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public String getSpeciesURL() {
        return species.get("url");
    }

    protected PokemonResume(PokemonResume other){
        super((Pokemon) other);
        this.types = other.types;
        this.abilities = other.abilities;
        this.weight = other.weight;
        this.species = other.species;
        this.sprites = other.getSprites();
    }

}
