package cl.enmala.api.pokedex.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sprites {
    @JsonProperty("front_default")
    private String frontURL;
    @JsonProperty("back_default")
    private String backURL;
}
