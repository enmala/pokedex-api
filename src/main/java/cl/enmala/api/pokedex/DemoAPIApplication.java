package cl.enmala.api.pokedex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import java.util.TimeZone;

@SpringBootApplication
@EnableCaching
public class DemoAPIApplication {

    public static void main(String[] args) {
        //Setting UTC as default time zone
        var utcTimeZone = TimeZone.getTimeZone("GMT");
        TimeZone.setDefault(utcTimeZone);
        
        SpringApplication.run(DemoAPIApplication.class, args);
    }

}
