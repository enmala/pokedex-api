package cl.enmala.api.pokedex.controller;

import cl.enmala.api.pokedex.dto.PokemonDetail;
import cl.enmala.api.pokedex.dto.PokemonList;
import cl.enmala.api.pokedex.dto.PokemonResume;
import cl.enmala.api.pokedex.service.PokemonService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Pokedex", description = "Servicios para obtener información de Pokemones")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class PokedexController {
    private final PokemonService pokemonService;

    @GetMapping("/")
    @Operation(summary = "Retorna una página con una lista de pokemones")
    public ResponseEntity<PokemonList> getPokemonPage(
            @RequestParam(value = "start",required = false,defaultValue = "1")
                    int start,
            @RequestParam(value = "count", required = false, defaultValue = "20")
                    int count
    ){
        var page = pokemonService.getPokemonPage(start,count);
        if(page == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        else
            return ResponseEntity.ok(page);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Retorna los datos básicos de un pokemón",
        responses = {
                @ApiResponse(responseCode = "200", content = @Content(
                    schema = @Schema(oneOf = {PokemonResume.class})
            )),
                @ApiResponse(responseCode = "404",description = "Pokemón no encontrado",
                content = @Content(schema = @Schema(hidden = true)))
        })
    public ResponseEntity<PokemonResume> getPokemon(
            @PathVariable("id") int id
    ) {
        var pokemon = pokemonService.getPokemon(id);
        if(pokemon.isPresent())
            return ResponseEntity.ok(pokemon.get());
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping("/{id}/details")
    @Operation(summary = "Retorna los datos completos de un pokemón",
            responses = {
                    @ApiResponse(responseCode = "200", content = @Content(
                            schema = @Schema(oneOf = {PokemonDetail.class})
                    )),
                    @ApiResponse(responseCode = "404",description = "Pokemón no encontrado",
                            content = @Content(schema = @Schema(hidden = true)))
            })
    public ResponseEntity<PokemonResume> getPokemonDetails(
            @PathVariable("id") int id
    ) {
        var pokemon = pokemonService.getPokemonDetail(id);
        if(pokemon.isPresent())
            return ResponseEntity.ok(pokemon.get());
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping(value = "/{id}/picture",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Operation(summary = "Retorna la imágen de un pokemón")
    public ResponseEntity<byte[]> getPokemonImage(
            @PathVariable("id") int id
    ) {
        var pokemon = pokemonService.getPokemonPicture(id);
        return pokemon;
    }
}
