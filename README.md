# Pokedex-API

Este proyecto es una ejemplo de API basada en Spring Boot.

Los datos se obtienen de https://pokeapi.co/

La aplicación expone los siguientes servicios:
- api/ :
    - Obtiene un listado de los pokemos existentes.
    - Opcionalmente se pueden pasar los parámetros:
        - start: número del pokemón inicial.
        - count: cantidad de pokemones a recuperar.
- api/{id} : 
    - Permite obtener los datos básicos de un pokemón
        - id
        - nombre
        - tipo
        - habilidades
        - peso
- api/{id}/details : 
    - permite obtener los datos completos de un pokemón. Ademas de los datos anteriores se obtiene:
        - evoluciones: lista de otros pokemón de los que puede evoluciona o a los que puede evolucionar.
        - descripción: No esta disponible. No encontré un atributo en la api de https://pokeapi.co/ que me pareciera adecuado.
- api/{id}/picture :
    - Obtiene la imagen del pokemón

# Documentación API
Para facilitar el uso de la API se ha implementado la documentación utilizando OpenAPI (Swagger). 

La documentación puede ser consultada en línea, directamente desde la raíz la API:
* https://pokedex-api.bitslab.cl/

Adicionalmente se dispone:
1. Archivo Postman con una colección de llamadas de ejemplo: 
   [Pokedex_API.postman_collection.json](Pokedex_API.postman_collection.json)
2. Aplicación de ejemplo que consume la API:
    - Disponible en línea en: https://enmala.gitlab.io/pokedex-demo/
    - Código fuente en: https://gitlab.com/enmala/pokedex-demo

# CI/CD 
- La aplicación se publica en forma automática utilizando pipelines de CI/CD de Gitlab.
- La configuración está en el archivo [.gitlab-ci.yml](.gitlab-ci.yml)
- La configuración CI/CD considera 4 etapas:
    - prebuild: Se descargan las dependencias de maven y se almacenan en el cache del sistema.
    - test: Se ejecutan las pruebas incorporadas en el código.
    - build: Se genera una imagen OCI (Docker) y se almacena en un repositorio en Google Cloud
    - deploy: Se ejecuta la imagen utilizando Google Cloud Run.
- Las etapas prebuild y test se ejecutan cada vez que se hace un push.
- Las etapas build y deploy se ejecutan solamente cuando se asigna una etiqueta (tag) en el repositorio.

# Monitoreo de errores
Se ha agregado [Sentry](https://sentry.io/) al proyecto para facilitar el monitoreo de errores en tiempo de ejecución.

# Ambiente de ejecución
Como se mencionó en la sección anterior, el proyecto esta ejecutando en Google Cloud utilizando Cloud Run. 

Adicionalmente se configuró un balanceador de carga y se agregó la generación automática de un certificado SSL.

**El proyecto se encuentra disponible en: https://pokedex-api.bitslab.cl/**

